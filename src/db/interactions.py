from db import PostgreSQLConnection, User, Role, Category, Base, MetaInformation, CategoryMetaInformation, Cart, \
    Product, ProductMetaInformation, CartProduct, Comment
from faker import Factory
from utils import DefaultProvider


class DbInteractions:

    def __init__(self, host, port, user, password, db_name, rebuild_db=False):
        self.connection = PostgreSQLConnection(
            host=host,
            port=port,
            user=user,
            password=password,
            db_name=db_name,
            rebuild_db=rebuild_db
        )

        self.engine = self.connection.connection.engine
        self.fake = Factory.create('ru_RU')
        self.fake.add_provider(DefaultProvider)

    def recreate_tables(self):
        for table in Base.metadata.tables.keys():
            self.connection.execute_query(f'DROP TABLE IF EXISTS "{table}" CASCADE')
            Base.metadata.tables[f'{table}'].create(self.engine)

    def add_some_role_instances(self):
        for _ in range(self.fake.random.randint(5, 10)):
            role_instance = Role(
                name=self.fake.job().title(),
                permissions={
                    table: {'r': self.fake.random.choice([True, False]), 'w': self.fake.random.choice([True, False]),
                            'x': self.fake.random.choice([True, False])} for table in Base.metadata.tables}
            )
            self.connection.session.add(role_instance)
            self.connection.session.commit()

    def add_some_user_instances(self):
        for _ in range(self.fake.random.randint(20, 30)):
            user_instance = User(
                first_name=self.fake.first_name(),
                second_name=self.fake.last_name(),
                email=self.fake.email(domain='gmail.com'),
                password=self.fake.password(),
                date_of_birth=self.fake.date(),
                place_of_birth=self.fake.city(),
                public_address=self.fake.address(),
                role_id=self.fake.random.randint(1, 5)
            )
            self.connection.session.add(user_instance)
            self.connection.session.commit()

    def add_some_category_instances(self):
        for i in range(5):
            category_instance = Category(
                name=self.fake.random_category(),
                parent_id=self.fake.random.randint(1, i) if i > 0 else None,
                type=self.fake.random.choice(['В тренде', 'Скидка', 'Черная пятница'])
            )
            self.connection.session.add(category_instance)
            self.connection.session.commit()

    def add_some_meta_information_instances(self):
        for _ in range(5):
            random_meta_information = self.fake.random_meta_information()
            meta_information_instance = MetaInformation(
                value_type=random_meta_information['value_type'],
                description=random_meta_information['description'],
                name=random_meta_information['name']
            )
            self.connection.session.add(meta_information_instance)
            self.connection.session.commit()

    def add_some_category_meta_information_instances(self):
        for i in range(5):
            for j in range(self.fake.random.randint(3, 5)):
                category_meta_information_instance = CategoryMetaInformation(
                    category_id=i + 1,
                    meta_information_id=j + 1
                )
                self.connection.session.add(category_meta_information_instance)
                self.connection.session.commit()

    def add_some_cart_instances(self):
        user_instances_count = self.connection.session.query(User).count()
        for _ in range(20, 30):
            random_user_id = self.fake.random.randint(1, user_instances_count)
            some_cart_instance = Cart(
                name=f'Корзина пользователя {random_user_id}',
                invoice_id=self.fake.random.randint(1, 10000) + self.fake.random.randint(1, 100),
                payment_url=self.fake.url() + ''.join(self.fake.random_letters()),
                payment_receipt_url=self.fake.url() + ''.join(self.fake.random_letters()),
                user_id=random_user_id
            )
            self.connection.session.add(some_cart_instance)
            self.connection.session.commit()

    def add_some_product_instances(self):
        user_instances_count = self.connection.session.query(User).count()
        category_instances_count = self.connection.session.query(Category).count()
        en_faker = Factory.create(locale='en_PH')
        for _ in range(20, 30):
            random_vendor_id = self.fake.random.randint(1, user_instances_count)
            random_category_id = self.fake.random.randint(1, category_instances_count)
            product_instance = Product(
                name=en_faker.random_company_product(),
                category_id=random_category_id,
                vendor_id=random_vendor_id
            )
            self.connection.session.add(product_instance)
            self.connection.session.commit()

    def add_some_product_meta_information_instances(self):
        product_instances_count = self.connection.session.query(Product).count()
        for i in range(product_instances_count):
            for j in range(5):
                meta_information_instance = self.connection.session.query(MetaInformation).filter_by(id=j + 1).first()
                if meta_information_instance.value_type == 'int':
                    random_value = str(self.fake.random.randint(100, 100000))
                elif meta_information_instance.value_type == 'float':
                    random_value = f'{str(self.fake.random.randint(100, 100000))}.{str(self.fake.random.randint(100, 200))}'
                else:
                    random_value = self.fake.color_name()
                product_meta_information_instance = ProductMetaInformation(
                    product_id=i + 1,
                    meta_information_id=j + 1,
                    value=random_value
                )
                self.connection.session.add(product_meta_information_instance)
                self.connection.session.commit()

    def add_some_cart_product_instances(self):
        product_instances_count = self.connection.session.query(Product).count()
        cart_instance_count = self.connection.session.query(Cart).count()
        for i in range(cart_instance_count):
            for _ in range(self.fake.random.randint(3, 10)):
                cart_product_instance = CartProduct(
                    cart_id=i + 1,
                    product_id=self.fake.random.randint(1, product_instances_count),
                    quantity=f'{str(self.fake.random.randint(100, 1000000))}.{str(self.fake.random.randint(100, 200))}'
                )
                self.connection.session.add(cart_product_instance)
                self.connection.session.commit()

    def add_some_comment_instances(self):
        product_instances_count = self.connection.session.query(Product).count()
        user_instances_count = self.connection.session.query(User).count()
        for _ in range(self.fake.random.randint(30, 100)):
            comment_instance = Comment(
                text=self.fake.text(),
                score=self.fake.random.randint(0, 5),
                product_id=self.fake.random.randint(1, product_instances_count),
                user_id=self.fake.random.randint(1, user_instances_count)
            )
            self.connection.session.add(comment_instance)
            self.connection.session.commit()