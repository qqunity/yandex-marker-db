from sqlalchemy import Column, Integer, ForeignKey, VARCHAR, DateTime, Text, UniqueConstraint, Numeric, \
    CheckConstraint, JSON
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

Base = declarative_base()


class Category(Base):
    __tablename__ = 'category'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(300), nullable=False)
    parent_id = Column(Integer, ForeignKey('category.id', ondelete='SET NULL'), nullable=True, index=True)
    type = Column(VARCHAR(300), nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), server_default=func.now())

    parent = relationship('Category', backref='children', remote_side=id)

    def __repr__(self):
        return f'<Category #{self.id} name="{self.name}">'


class MetaInformation(Base):
    __tablename__ = 'meta_information'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    value_type = Column(VARCHAR(300), nullable=False)
    description = Column(Text, nullable=True)
    name = Column(VARCHAR(500), nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), server_default=func.now())

    UniqueConstraint(name, name='name')

    def __repr__(self):
        return f'<MetaInformation #{self.id} name="{self.name}" value_type="{self.value_type}">'


class CategoryMetaInformation(Base):
    __tablename__ = 'category_meta_information'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    category_id = Column(Integer, ForeignKey('category.id', ondelete='CASCADE'), nullable=False, index=True)
    meta_information_id = Column(Integer, ForeignKey('meta_information.id', ondelete='CASCADE'), nullable=False, index=True)

    category = relationship('Category', backref='categories')
    meta_information = relationship('MetaInformation', backref='meta_information')

    def __repr__(self):
        return f'<CategoryMetaInformation #{self.id} category_id="{self.category_id}" meta_information_id="{self.meta_information_id}">'


class Role(Base):
    __tablename__ = 'role'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(500), nullable=False)
    permissions = Column(JSON, nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), server_default=func.now())

    def __repr__(self):
        return f'<Role #{self.id} name="{self.name}" permissions="{self.permissions}">'


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    first_name = Column(VARCHAR(300), nullable=True)
    second_name = Column(VARCHAR(300), nullable=True)
    email = Column(VARCHAR(500), CheckConstraint(r"email ~ '^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$'"), nullable=False)
    password = Column(VARCHAR(500), nullable=False)
    date_of_birth = Column(DateTime(timezone=True), nullable=True)
    place_of_birth = Column(VARCHAR(1000), nullable=True)
    public_address = Column(VARCHAR(1000), nullable=True)
    role_id = Column(Integer, ForeignKey('role.id', ondelete='CASCADE'), nullable=False, index=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), server_default=func.now())

    role = relationship('Role', backref='users')

    UniqueConstraint(email, name='email')

    def __repr__(self):
        return f'<User #{self.id} email="{self.email}">'


class Cart(Base):
    __tablename__ = 'cart'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(500), nullable=False)
    invoice_id = Column(Integer, nullable=True)
    payment_url = Column(VARCHAR(500), nullable=True)
    payment_receipt_url = Column(VARCHAR(500), nullable=True)
    user_id = Column(Integer, ForeignKey('user.id', ondelete='CASCADE'), nullable=False, index=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), server_default=func.now())

    user = relationship('User', backref='carts')

    UniqueConstraint(invoice_id, name='invoice_id')
    UniqueConstraint(payment_url, name='payment_url')
    UniqueConstraint(payment_receipt_url, name='payment_receipt_url')

    def __repr__(self):
        return f'<Cart #{self.id} name="{self.name}">'


class Product(Base):
    __tablename__ = 'product'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    name = Column(VARCHAR(1000), nullable=False)
    category_id = Column(Integer, ForeignKey('category.id', ondelete='CASCADE'), nullable=False, index=True)
    vendor_id = Column(Integer, ForeignKey('user.id', ondelete='SET NULL'), nullable=False, index=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), server_default=func.now())

    category = relationship('Category', backref='products')
    vendor = relationship('User', backref='products')

    def __repr__(self):
        return f'<Product #{self.id} name="{self.name}">'


class ProductMetaInformation(Base):
    __tablename__ = 'product_meta_information'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    product_id = Column(Integer, ForeignKey('product.id', ondelete='CASCADE'), nullable=False, index=True)
    meta_information_id = Column(Integer, ForeignKey('meta_information.id', ondelete='CASCADE'), nullable=False, index=True)
    value = Column(VARCHAR(1500), nullable=False)

    product = relationship('Product', backref='product_meta_information')
    meta_information = relationship('MetaInformation', backref='product_meta_information')

    def __repr__(self):
        return f'<ProductMetaInformation #{self.id} product_id="{self.product_id}" meta_information_id="{self.meta_information_id}" value="{self.value}">'


class CartProduct(Base):
    __tablename__ = 'cart_product'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    product_id = Column(Integer, ForeignKey('product.id', ondelete='CASCADE'), nullable=False, index=True)
    cart_id = Column(Integer, ForeignKey('cart.id', ondelete='CASCADE'), nullable=False, index=True)
    quantity = Column(Numeric(10, 3), CheckConstraint('quantity > 0'), nullable=False)

    product = relationship('Product', backref='cart_product')
    cart = relationship('Cart', backref='cart_product')

    def __repr__(self):
        return f'<CartProduct #{self.id}> product_id="{self.product_id}" cart_id="{self.cart_id}" quantity="{self.quantity}"'


class Comment(Base):
    __tablename__ = 'comment'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    text = Column(Text, nullable=False)
    score = Column(Integer, CheckConstraint('score >= 0 AND score <= 10'), nullable=False)
    product_id = Column(Integer, ForeignKey('product.id', ondelete='SET NULL'), nullable=False, index=True)
    user_id = Column(Integer, ForeignKey('user.id', ondelete='SET NULL'), nullable=False, index=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), server_default=func.now())

    product = relationship('Product', backref='comments', cascade='all,delete')
    user = relationship('User', backref='comments', cascade='all,delete')

    def __repr__(self):
        return f'<Comment #{self.id} user_id="{self.user_id}" product_id="{self.product_id}" score="{self.score}">'
