-- Выберем все подкатегории которые относятся к "Планшетам"
--  Добавить агрег функцию -> (Сложный) !!!
WITH RECURSIVE r AS (
    SELECT id, parent_id, type, name, 1 AS level
    FROM category
    WHERE name = 'Планшеты'

    UNION ALL

    SELECT category.id, category.parent_id, category.type, category.name, r.level + 1 AS level
    FROM category
             JOIN r
                  ON category.parent_id = r.id
)

SELECT *
FROM r
         JOIN (SELECT count(*) AS category_count, level FROM r GROUP BY level) sub_q ON r.level = sub_q.level
ORDER BY r.level;

-- Выберем все идентификаторы конечных подкатегорий, затем для них количество и сумму товаров
-- Простой -> (Сложный) !!!
WITH sub_q AS (
    SELECT id
    FROM category
    WHERE id NOT IN (SELECT DISTINCT p.id
                     FROM category c
                              INNER JOIN category AS p ON c.parent_id = p.id))

SELECT p.id, mi.name, SUM(CAST(pmi.value AS DECIMAL))
FROM product p
         JOIN product_meta_information pmi ON p.id = pmi.product_id
         JOIN meta_information mi ON pmi.meta_information_id = mi.id
WHERE (mi.name = 'quantity' OR mi.name = 'price')
  AND p.category_id IN (SELECT * FROM sub_q)
GROUP BY p.id, mi.name;

-- Выберем все наименования товаров в конечных подкатегориях
-- Средний
WITH sub_q AS (SELECT id
               FROM category
               WHERE id NOT IN (SELECT DISTINCT p.id
                                FROM category c
                                         JOIN category AS p ON c.parent_id = p.id))

SELECT p.name
FROM product p
         JOIN category c ON p.category_id = c.id
WHERE c.id IN (SELECT * FROM sub_q);

-- Выберем все товары с оценкой больше 3 и всех пользователей писавшик эти комментарии
-- Простой -> (Средний) !!!
WITH sub_q AS (
    SELECT p.id
    FROM product p
             JOIN comment c ON p.id = c.product_id
    WHERE c.score > 3
    GROUP BY p.id, p.name)
SELECT product_id, user_id
FROM comment
WHERE product_id IN (SELECT * FROM sub_q);


-- Товары, которые закончились на складе
-- Сложный
WITH sub_q1 AS (SELECT p.id, p.name, SUM(CAST(pmi.value AS DECIMAL)) AS total_quantity
                FROM product_meta_information pmi
                         JOIN product p ON pmi.product_id = p.id
                         JOIN meta_information mi ON pmi.meta_information_id = mi.id
                WHERE mi.name = 'quantity'
                GROUP BY p.id, p.name)
SELECT sub_q1.id, sub_q1.name
FROM sub_q1
         JOIN (SELECT p.id, p.name, SUM(cp.quantity) AS planned_total_quantity
               FROM product p
                        JOIN cart_product cp ON p.id = cp.product_id
               GROUP BY p.id, p.name) sub_q2 ON sub_q1.id = sub_q2.id
WHERE sub_q1.total_quantity <= sub_q2.planned_total_quantity;

-- Пользователи c общим количеством комментариев, которые написали хотя бы один плохой комментарий (оценка меньше или равна 2)
-- Средний
WITH sub_q AS (SELECT DISTINCT u.id
               FROM "user" u
                        JOIN comment c ON u.id = c.user_id
               WHERE c.score <= 2)
SELECT u.id, u.first_name, u.second_name, COUNT(*) AS comment_count
FROM "user" u
         JOIN comment c ON u.id = c.user_id
WHERE u.id IN (SELECT * FROM sub_q)
GROUP BY u.id;

-- Самый дорогой товар c с его количеством
-- Средний/сложный -> (Сложный) !!!
WITH sub_q AS (SELECT MAX(CAST(pmi.value AS DECIMAL))
               FROM product p
                        JOIN product_meta_information pmi ON p.id = pmi.product_id
                        JOIN meta_information mi ON pmi.meta_information_id = mi.id
               WHERE mi.name = 'price')
SELECT p.id, p.name, total_count, pmi.value AS price
FROM product p
         JOIN (SELECT p.id, SUM(CAST(pmi.value AS DECIMAL)) AS total_count
               FROM product p
                        JOIN product_meta_information pmi ON p.id = pmi.product_id
                        JOIN meta_information mi ON pmi.meta_information_id = mi.id
               WHERE mi.name = 'quantity'
               GROUP BY p.id, mi.name
) sub_sub_q ON sub_sub_q.id = p.id
         JOIN product_meta_information pmi ON p.id = pmi.product_id
         JOIN meta_information mi ON pmi.meta_information_id = mi.id
WHERE mi.name = 'price'
  AND pmi.value = CAST((SELECT * FROM sub_q) AS VARCHAR);

-- Категории, у товаров которых не присутсвует размер обуви (meta_information_id = 4), при этом есть товары дороже 7_000
-- Средний -> (Сложный) !!!
WITH sub_q AS (
    WITH sub_sub_q AS (
        SELECT DISTINCT(p.category_id)
        FROM (SELECT p.id, SUM(CAST(pmi.value AS DECIMAL)) AS total_price
              FROM product p
                       JOIN product_meta_information pmi ON p.id = pmi.product_id
                       JOIN meta_information mi ON pmi.meta_information_id = mi.id
              WHERE mi.name = 'price'
              GROUP BY p.id) sub_sub_sub_q
                 JOIN product p ON p.id = sub_sub_sub_q.id
        WHERE total_price < 7000
    )

    SELECT c.id
    FROM category c
             JOIN category_meta_information cmi ON c.id = cmi.category_id
    WHERE cmi.meta_information_id = 4
      AND c.id IN (SELECT * FROM sub_sub_q)
)

SELECT c.id, c.name
FROM category c
WHERE c.id NOT IN (SELECT * FROM sub_q);

-- Роли, который являются поставщиками товаров
-- Простой
SELECT DISTINCT r.id, r.name
FROM product p
         JOIN "user" u ON u.id = p.vendor_id
         JOIN role r ON r.id = u.role_id;

-- Корзины пользователей с размером ноги меньше 30_000 условных единиц
-- Средний
SELECT DISTINCT c.id, c.name
FROM cart c
         JOIN cart_product cp ON c.id = cp.cart_id
         JOIN product p ON cp.product_id = p.id
         JOIN product_meta_information pmi ON p.id = pmi.product_id
         JOIN meta_information mi ON mi.id = pmi.meta_information_id
WHERE mi.name = 'shoe_size'
  AND CAST(pmi.value AS DECIMAL) < CAST('30000' AS DECIMAL);

-- Пользователь, который потратил больше всех денег на сайте
-- Сложный
WITH sub_sub_q AS (WITH sub_q AS (SELECT c.id,
                                         c.user_id,
                                         p.id                                          AS product_id,
                                         SUM(cp.quantity) * CAST(pmi.value AS DECIMAL) AS product_price
                                  FROM cart c
                                           JOIN cart_product cp ON c.id = cp.cart_id
                                           JOIN product p ON cp.product_id = p.id
                                           JOIN product_meta_information pmi ON p.id = pmi.product_id
                                           JOIN meta_information mi ON mi.id = pmi.meta_information_id
                                  WHERE mi.name = 'price'
                                  GROUP BY c.id, user_id, p.id, pmi.value)

                   SELECT id, user_id, SUM(product_price) AS total_price
                   FROM sub_q
                   GROUP BY id, user_id)
SELECT user_id
FROM sub_sub_q
WHERE total_price = (SELECT MAX(total_price)
                     FROM sub_sub_q);

-- Все ссылки на чек для пользователей
-- Средний
SELECT u.id, ARRAY_AGG(c.payment_receipt_url) AS receipt_urls
FROM "user" u
         JOIN cart c ON u.id = c.user_id
GROUP BY u.id;

-- Самые длинные комментарий
-- Простой
SELECT c.id, LENGTH(c.text) AS text_length
FROM comment c
WHERE LENGTH(c.text) = (SELECT MAX(LENGTH(c.text)) FROM comment c);

-- Все желто-зеленый товары с количетвом
-- Простой -> (Средний) !!!
WITH sub_q AS (
    SELECT p.id, SUM(CAST(pmi.value AS DECIMAL)) AS total_quantity
    FROM product p
             JOIN product_meta_information pmi on p.id = pmi.product_id
             JOIN meta_information mi ON pmi.meta_information_id = mi.id
    WHERE mi.name = 'quantity'
    GROUP BY p.id
)
SELECT p.id, p.name, total_quantity
FROM product p
         JOIN product_meta_information pmi ON p.id = pmi.product_id
         JOIN meta_information mi ON mi.id = pmi.meta_information_id
         JOIN sub_q ON p.id = sub_q.id
WHERE mi.name = 'color'
  AND pmi.value = 'Желто-зеленый';

-- Пользователей ничего не заказавших
-- Простой
SELECT u.id, u.first_name, u.second_name
FROM "user" u
         LEFT JOIN cart c ON u.id = c.user_id
WHERE c.id is NULL;

-- Выбор самого последнего заказа и пидущего перед ним по конкретной роли пользователя и вычисление разницы в стоимости
-- Сложный (добавить)
SELECT c.id                       AS cart_id,
       c.user_id,
       r.name,
       c.created_at,
       LAG(c.id) OVER (PARTITION BY r.name
           ORDER BY c.created_at) AS previous_cart_id
FROM cart c
         JOIN "user" u ON u.id = c.user_id
         JOIN role r ON r.id = u.role_id;
