from faker.providers import BaseProvider


class DefaultProvider(BaseProvider):
    _categories = iter([
        'Планшеты',
        'Телефоны',
        'Персональные компьютеры',
        'Бытовая химия',
        'Продукты',
        'Зоотовары'
    ])

    _meta_information = iter([
        {
            'value_type': 'float',
            'description': 'Цена в рублях',
            'name': 'price'
        },
        {
            'value_type': 'float',
            'description': 'Вес в граммах',
            'name': 'weight'
        },
        {
            'value_type': 'str',
            'description': 'Цвет',
            'name': 'color'
        },
        {
            'value_type': 'int',
            'description': 'Европейский размер обуви',
            'name': 'shoe_size'
        },
        {
            'value_type': 'int',
            'description': 'Количество товара в штуках',
            'name': 'quantity'
        }
    ])

    def random_category(self) -> str:
        return next(self._categories)

    def random_meta_information(self):
        return next(self._meta_information)
