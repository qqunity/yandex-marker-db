create table category
(
	id serial
		constraint category_pkey
			primary key,
	name varchar(300) not null,
	parent_id integer
		constraint category_parent_id_fkey
			references category
				on delete set null,
	type varchar(300) not null,
	created_at timestamp with time zone default now(),
	updated_at timestamp with time zone default now()
);

alter table category owner to postgres;

create index ix_category_parent_id
	on category (parent_id);

create table meta_information
(
	id serial
		constraint meta_information_pkey
			primary key,
	value_type varchar(300) not null,
	description text,
	name varchar(500) not null
		constraint name
			unique,
	created_at timestamp with time zone default now(),
	updated_at timestamp with time zone default now()
);

alter table meta_information owner to postgres;

create table category_meta_information
(
	id serial
		constraint category_meta_information_pkey
			primary key,
	category_id integer not null
		constraint category_meta_information_category_id_fkey
			references category
				on delete cascade,
	meta_information_id integer not null
		constraint category_meta_information_meta_information_id_fkey
			references meta_information
				on delete cascade
);

alter table category_meta_information owner to postgres;

create index ix_category_meta_information_category_id
	on category_meta_information (category_id);

create index ix_category_meta_information_meta_information_id
	on category_meta_information (meta_information_id);

create table role
(
	id serial
		constraint role_pkey
			primary key,
	name varchar(500) not null,
	permissions json not null,
	created_at timestamp with time zone default now(),
	updated_at timestamp with time zone default now()
);

alter table role owner to postgres;

create table "user"
(
	id serial
		constraint user_pkey
			primary key,
	first_name varchar(300),
	second_name varchar(300),
	email varchar(500) not null
		constraint email
			unique
		constraint user_email_check
			check ((email)::text ~ '^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$'::text),
	password varchar(500) not null,
	date_of_birth timestamp with time zone,
	place_of_birth varchar(1000),
	public_address varchar(1000),
	role_id integer not null
		constraint user_role_id_fkey
			references role
				on delete cascade,
	created_at timestamp with time zone default now(),
	updated_at timestamp with time zone default now()
);

alter table "user" owner to postgres;

create index ix_user_role_id
	on "user" (role_id);

create table cart
(
	id serial
		constraint cart_pkey
			primary key,
	name varchar(500) not null,
	invoice_id integer
		constraint invoice_id
			unique,
	payment_url varchar(500)
		constraint payment_url
			unique,
	payment_receipt_url varchar(500)
		constraint payment_receipt_url
			unique,
	user_id integer not null
		constraint cart_user_id_fkey
			references "user"
				on delete cascade,
	created_at timestamp with time zone default now(),
	updated_at timestamp with time zone default now()
);

alter table cart owner to postgres;

create index ix_cart_user_id
	on cart (user_id);

create table product
(
	id serial
		constraint product_pkey
			primary key,
	name varchar(1000) not null,
	category_id integer not null
		constraint product_category_id_fkey
			references category
				on delete cascade,
	vendor_id integer not null
		constraint product_vendor_id_fkey
			references "user"
				on delete set null,
	created_at timestamp with time zone default now(),
	updated_at timestamp with time zone default now()
);

alter table product owner to postgres;

create index ix_product_vendor_id
	on product (vendor_id);

create index ix_product_category_id
	on product (category_id);

create table product_meta_information
(
	id serial
		constraint product_meta_information_pkey
			primary key,
	product_id integer not null
		constraint product_meta_information_product_id_fkey
			references product
				on delete cascade,
	meta_information_id integer not null
		constraint product_meta_information_meta_information_id_fkey
			references meta_information
				on delete cascade,
	value varchar(1500) not null
);

alter table product_meta_information owner to postgres;

create index ix_product_meta_information_meta_information_id
	on product_meta_information (meta_information_id);

create index ix_product_meta_information_product_id
	on product_meta_information (product_id);

create table cart_product
(
	id serial
		constraint cart_product_pkey
			primary key,
	product_id integer not null
		constraint cart_product_product_id_fkey
			references product
				on delete cascade,
	cart_id integer not null
		constraint cart_product_cart_id_fkey
			references cart
				on delete cascade,
	quantity numeric(4) not null
		constraint cart_product_quantity_check
			check (quantity > (0)::numeric)
);

alter table cart_product owner to postgres;

create index ix_cart_product_cart_id
	on cart_product (cart_id);

create index ix_cart_product_product_id
	on cart_product (product_id);

create table comment
(
	id serial
		constraint comment_pkey
			primary key,
	text text not null,
	score integer not null
		constraint comment_score_check
			check ((score >= 0) AND (score <= 10)),
	product_id integer not null
		constraint comment_product_id_fkey
			references product
				on delete set null,
	user_id integer not null
		constraint comment_user_id_fkey
			references "user"
				on delete set null,
	created_at timestamp with time zone default now(),
	updated_at timestamp with time zone default now()
);

alter table comment owner to postgres;

create index ix_comment_product_id
	on comment (product_id);

create index ix_comment_user_id
	on comment (user_id);

