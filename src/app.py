from fastapi import FastAPI
from db import DbInteractions

app = FastAPI()


@app.get("/")
async def root():
    db_interactions = DbInteractions(
        host='db',
        port='5434',
        user='postgres',
        password='postgres',
        db_name='postgres',
        rebuild_db=False
    )
    # db_interactions.recreate_tables()
    #
    # db_interactions.add_some_role_instances()
    # db_interactions.add_some_user_instances()
    # db_interactions.add_some_category_instances()
    # db_interactions.add_some_meta_information_instances()
    # db_interactions.add_some_category_meta_information_instances()
    # db_interactions.add_some_cart_instances()
    # db_interactions.add_some_product_instances()
    # db_interactions.add_some_product_meta_information_instances()
    # db_interactions.add_some_cart_product_instances()
    # db_interactions.add_some_comment_instances()

    return {"message": "Data loaded successfully!"}
